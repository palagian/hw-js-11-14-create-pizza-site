document.addEventListener("DOMContentLoaded", () => {

    // прописываем цены на размер пиццы, соусы и топпинги
    const price = {
        small: 100,
        mid: 125,
        big: 150,
        ketchup: 15,
        bbq: 20,
        rikotta: 25,
        cheese: 10,
        feta: 15,
        mozarella: 20,
        telya: 25,
        tomatoes: 15,
        mushrooms: 15
    };

    // создаем объект для пиццы клиента
    const pizza = {
        size: null,
        sauce: [],
        nameSauce: [],
        topping: [],
        nameTopping: [],
        totalPrice: 0,
    };

    // функция для выбора размера пиццы
    const pizzaSize = document.getElementById("pizza");
    pizzaSize.addEventListener('click', (e) => {
        pizza.size = e.target.value;
        yourOrder();
    })

    //считаем сумму выбранного состава пиццы
    const totalPrice = () => {
        // обнуляем цену, если кликов несколько
        if (pizza.totalPrice != 0) {
            pizza.totalPrice = 0;
        }

        // считаем цену размера пиццы и добавляем в общую сумму
        switch (pizza.size) {
            case "small":
                pizza.totalPrice += price.small;
                break;
            case "mid":
                pizza.totalPrice += price.mid;
                break;
            case "big":
                pizza.totalPrice += price.big;
                break;
        }

        // считаем цену соусов и добавляем в общую сумму
        pizza.sauce.forEach(el => {
            switch (el) {
                case "sauceClassic":
                    pizza.totalPrice += price.ketchup;
                    break;
                case "sauceBBQ":
                    pizza.totalPrice += price.bbq;
                    break;
                case "sauceRikotta":
                    pizza.totalPrice += price.rikotta;
                    break;
            }
        })

        // считаем цену топпингов и добавляем в общую сумму
        pizza.topping.forEach(el => {
            switch (el) {
                case "moc1":
                    pizza.totalPrice += price.cheese;
                    break;
                case "moc2":
                    pizza.totalPrice += price.feta;
                    break;
                case "moc3":
                    pizza.totalPrice += price.mozarella;
                    break;
                case "telya":
                    pizza.totalPrice += price.telya;
                    break;
                case "vetch1":
                    pizza.totalPrice += price.tomatoes;
                    break;
                case "vetch2":
                    pizza.totalPrice += price.mushrooms;
                    break;
            }
        })
    }

    // выводим цену на страницу
    const showPrice = () => {
        document.querySelector(".price > p").innerHTML = `Ціна: ${pizza.totalPrice}`;
    }

    // выводим список соусов на страницу    
    const showSauces = () => {
        document.querySelector(".sauces > p").innerHTML = `Соуси: ${pizza.nameSauce.join(", ")}`;
    }

    // выводим список топингов на страницу
    const showToppings = () => {
        document.querySelector(".topings > p").innerHTML = `Топінги: ${pizza.nameTopping.join(", ")}`;
    }

    // вызываем функции по выводу цен на экран
    const yourOrder = () => {
        totalPrice();
        showPrice();
        showSauces();
        showToppings();
    }

    // создаем функцию для перетаскивания элементов на корж
    const dragAndDrop = () => {
        const pizzaStart = document.querySelector(".table-wrapper .table");

        // начинаем перетаскивать элемент
        function dragStart(e) {
            // добавляем элементы в массивы с ингридиентами
            const item = e.target.dataset.key;
            if (item === "sauce") {
                if (!pizza.nameSauce.includes(e.target.alt)) {
                    pizza.nameSauce.push(e.target.alt);
                    pizza.sauce.push(e.target.id);
                }
            } else {
                if (!pizza.nameTopping.includes(e.target.alt)) {
                    pizza.nameTopping.push(e.target.alt);
                    pizza.topping.push(e.target.id);
                }
            }
            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setData("img", this.attributes.src.textContent);
        }

        // закончили перетаскивать элемент
        const dragEnd = (e) => {
            pizzaStart.classList.remove("hovered");
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
        }

        // перетаскиваемый элемент находится над целевым
        function dragOver(e) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
        }

        // перетаскиваемый элемент в области целевого
        function dragEnter(e) {
            pizzaStart.classList.add("hovered");
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
        }

        // перетаскиваемый элемент покинул область целевого
        function dragLeave(e) {
            pizzaStart.classList.remove("hovered");
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
        }

        // перетаскивание завершилось отпусканием элемента над целевым
        function dragDrop(e) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }

            const itemPic = document.createElement("img");
            itemPic.setAttribute("src", e.dataTransfer.getData("img"));
            pizzaStart.append(itemPic);

            yourOrder();
        }

        // обработчики для целевого элемента
        pizzaStart.addEventListener('dragover', dragOver);
        pizzaStart.addEventListener('drop', dragDrop);
        pizzaStart.addEventListener('dragenter', dragEnter);
        pizzaStart.addEventListener('dragleave', dragLeave);

        // обработчик для перетаскиваемого объекта
        const ingridients = document.querySelectorAll('.ingridients .draggable');
        ingridients.forEach(item => {
            item.addEventListener('dragstart', dragStart);
            item.addEventListener('dragend', dragEnd);
        })
    }
    dragAndDrop();

    // создаем событие на проверку полей с личной информацией
    const forms = document.forms[1];
    let validFlag = false;

    forms.addEventListener("change", () => {

        for (let i = 0; i < (forms.length - 1); i++) {
            if (forms[i].type === "text") {
                if (/^[А-яіїґє-]+$/.test(forms[i].value)) {
                    validFlag = true;
                }
                if (!/^[А-яіїґє-]+$/.test(forms[i].value)) {
                    validFlag = false;
                }
                if (!validFlag) {
                    break;
                }
            }
            if (forms[i].type === "tel") {
                if (/^\+380[0-9]{9}$/.test(forms[i].value)) {
                    validFlag = true;
                }
                if (!/^\+380[0-9]{9}$/.test(forms[i].value)) {
                    validFlag = false;
                }
                if (!validFlag) {
                    break;
                }
            }
            if (forms[i].type === "email") {
                if (/[ˆa-z0-9._]+@[a-z0-9.-]+\.[a-z]+$/.test(forms[i].value)) {
                    validFlag = true;
                }
                if (!/[ˆa-z0-9._]+@[a-z0-9.-]+\.[a-z]+$/.test(forms[i].value)) {
                    validFlag = false;
                }
                if (!validFlag) {
                    break;
                }
            }
        }
        return validFlag;
    })

    // валидация всей формы и событие на кнопку "Подтвердить заказ"
    const btnSumbit = document.querySelector("[type = submit]");

    btnSumbit.addEventListener("click", (e) => {
        e.preventDefault();
        if (validFlag && pizza.size !== null) {
            window.location.href = "./thank-you.html";
        }
        if (!validFlag && pizza.size !== null) {
            alert("Личная информация заполнена неверно!");
        }
        if (validFlag && pizza.size == null) {
            alert("Размер пиццы не выбран!");
        }
        if (!validFlag && pizza.size == null) {
            alert("Размер пиццы не выбран и личная информация заполнена неверно!");
        }
    })

    // событие для кнопки сброса
    const btnReset = document.querySelector("[type = reset]");

    btnReset.addEventListener("click", () => {

        // обнуляем объект с составом пиццы
        pizza.size = null;
        pizza.sauce = [];
        pizza.nameSauce = [];
        pizza.topping = [];
        pizza.nameTopping = [];
        pizza.totalPrice = 0;

        // убираем выведенные ингредиенты с макета пиццы
        const item = document.querySelectorAll('.table img');

        item.forEach(item => {
            if (!item.alt[0]) {
                item.remove();
            }
        });

        // обнуляем значения в полях с личной информацией
        for (let i = 0; i < forms.length; i++) {
            if (forms[i].type === "text" || forms[i].type === "tel" || forms[i].type === "email") {
                forms[i].value = "";
            }
        }
        yourOrder();
    })

    // убегающий баннер
    const discount = document.getElementById("banner");
    discount.addEventListener("mousemove", (e) => {
        banner.style.right = Math.random() * (document.documentElement.clientWidth - e.target.offsetWidth) + 'px';
		banner.style.bottom = Math.random() * (document.documentElement.clientHeight - e.target.offsetHeight) + 'px';
    })
})